import os

from discord import Embed
from discord.ext import commands
from easy_request import Radarr, Sonarr
from easy_request.exceptions import MovieAlreadyExists


class Request(commands.Cog):
    def __init__(self):
        self.radarr = Radarr(
            token=os.getenv("RADARR_TOKEN"), api_url=os.getenv("RADARR_URL")
        )
        self.sonarr = Sonarr(
            token=os.getenv("SONARR_TOKEN"), api_url=os.getenv("SONARR_URL")
        )

    @commands.command()
    async def request(self, ctx, content_type=None, *content_name):
        if content_type and content_name:
            content = None

            if content_type == "movie":
                try:
                    content = self.radarr.find_movies(term=content_name)
                    self.radarr.request_movie(movie=content[0])
                except MovieAlreadyExists:
                    content = None
                    await ctx.send("Movie is already added")
            elif content_type == "tv":
                try:
                    content = self.sonarr.find_series(term=content_name)
                    self.sonarr.request_series(series=content[0])
                except Exception:
                    content = None
                    await ctx.send("Series already exists")

            else:
                await ctx.send("Please Specify Content Type (movie/tv)")

            if content:
                embed = Embed(title=content[0]["title"])
                embed.add_field(name="Release Year", value=content[0]["year"])
                embed.add_field(
                    name="Overview", value=content[0]["overview"], inline=False
                )
                embed.set_image(url=content[0]["remotePoster"])
                embed.add_field(name="Status", value="Added", inline=False)
                await ctx.send(embed=embed)
        else:
            if not content_type:
                return ctx.send("Please specify if movie or tv")
            if not content_name:
                await ctx.send("Please Specify Content Name")
