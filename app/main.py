import os

from discord.ext.commands import Bot

from app.cogs.Request import Request

bot = Bot(command_prefix="!")


@bot.event
async def on_ready():
    print(f"I am {bot.user.name}")
    bot.add_cog(Request())

bot.run(os.getenv('DISCORD_API'))
