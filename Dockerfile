FROM python:slim-buster

ENV PIPENV_IGNORE_VIRTUALENVS=1

WORKDIR app

RUN pip install pipenv

COPY . .

RUN pipenv install

CMD ["pipenv", "run", "python", "app/main.py"]